CLASS zcl_test DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    "! Wrapper-Method for TR_TADIR_INTERFACE
    "! @parameter is_tadir | Current TADIR Objekt / Structure
    "! @parameter iv_srcsystem | New Source-System
    "! @parameter iv_author | New Author
    "! @parameter iv_test_modus | Test-Mode - just simulate, don't update
    "! @parameter ev_total | Number of objects
    "! @parameter ev_success | Number of successful updates
    "! @parameter et_success | Table containing successfully updated structures
    "! @parameter et_fail | Table containing failed updates
    CLASS-METHODS update_tadir
      IMPORTING it_tadir      TYPE tt_tadir
                iv_srcsystem  TYPE tadir-srcsystem
                iv_author     TYPE tadir-author
                iv_test_modus TYPE abap_bool DEFAULT abap_false
      EXPORTING ev_total      TYPE i
                ev_success    TYPE i
                et_success    TYPE tt_tadir
                et_fail       TYPE tt_tadir.

  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS ZCL_TEST IMPLEMENTATION.


  METHOD update_tadir.
    DATA: ls_tadir_new LIKE LINE OF et_success.

    LOOP AT it_tadir INTO DATA(ls_tadir).

      CALL FUNCTION 'TR_TADIR_INTERFACE'
        EXPORTING
          wi_remove_repair_flag = ' '
          wi_set_repair_flag    = ' '
          wi_test_modus         = iv_test_modus
          wi_tadir_pgmid        = ls_tadir-pgmid
          wi_tadir_object       = ls_tadir-object
          wi_tadir_obj_name     = ls_tadir-obj_name
          wi_tadir_srcsystem    = iv_srcsystem
          wi_tadir_author       = iv_author
          wi_remove_genflag     = ' '
          wi_set_genflag        = ' '
        IMPORTING
          new_tadir_entry       = ls_tadir_new
        EXCEPTIONS
          OTHERS                = 25.

      IF sy-subrc <> 0.
        APPEND ls_tadir TO et_fail.
      ELSE.
        APPEND ls_tadir_new TO et_success.
        ev_success = ev_success + 1.
      ENDIF.
      ev_total = ev_total + 1.
    ENDLOOP.
  ENDMETHOD.
ENDCLASS.
